# Databehandleraftale


## Baggrund for databehandleraftalen

1.  Denne aftale fastsætter de rettigheder og forpligtelser, som finder
    anvendelse, når Previsto foretager behandling af
    personoplysninger på vegne af Kunden.

2.  Aftalen er udformet med henblik på parternes efterlevelse af artikel
    28, stk. 3, i *Europa-Parlamentets og Rådets forordning (EU)
    2016/679 af 27. april 2016 om beskyttelse af fysiske personer i
    forbindelse med behandling af personoplysninger og om fri udveksling
    af sådanne oplysninger og om ophævelse af direktiv 95/46/EF
    (Databeskyttelsesforordningen)*, som stiller specifikke krav til
    indholdet af en databehandleraftale.

4.  Databehandleraftalen og [betingelser for anvendelse af Previstos](https://previsto.com/da/betingelser/) er 
    indbyrdes afhængige, og kan ikke opsiges særskilt. Databehandleraftalen kan dog erstattes af en anden gyldig
    databehandleraftale.

5.  Denne databehandleraftale har forrang i forhold til eventuelle
    tilsvarende bestemmelser i andre aftaler mellem parterne, herunder [betingelser for anvendelse af Previstos](https://previsto.com/da/betingelser/).

6.  Til denne aftale hører 2 bilag. Bilagene fungerer som en
    integreret del af databehandleraftalen.

7.  Kunden godkender at Previsto kan involvere underdatabehandlere til at 
    behandle personlige data på kundens vegne. Databehandleraftalens Bilag A 
    indeholder en liste over de eventuelle underdatabehandlere, som Kunden har 
    godkendt.

8.  Databehandleraftalens Bilag B indeholder en nærmere af Previstos
    sikkerhedsforanstaltninger.

9. Denne databehandleraftale frigør ikke Previsto for
    forpligtelser, som efter databeskyttelsesforordningen eller enhver
    anden lovgivning direkte er pålagt Previsto.

10. **Kundens forpligtelser:** Kunden har (i) overfor omverdenen (herunder den
    registrerede) som udgangs-punkt ansvaret for, at behandlingen af
    personoplysninger sker indenfor rammerne af
    databeskyttelsesforordningen og databeskyttelsesloven, (ii) både rettighederne og forpligtelserne
    til at træffe beslutninger om, til hvilke formål og med hvilke
    hjælpemidler der må foretages behandling, (iii) ansvar for, at der foreligger
    hjemmel til den behandling, som Previsto instrueres i at
    foretage.

11. **Dataprocessering:** Previsto behandler kun personoplysninger efter
    instruks fra Kunden, medmindre det kræves i
    henhold til EU-ret eller medlemsstaternes nationale ret, som
    Previsto er underlagt.


## Behandlingssikkerhed 
1. **Sikkerhedsforanstaltninger:** Previsto skal sørge for passende tekniske og organisatoriske 
sikkerhedsforanstaltninger til beskyttelse af personoplysninger fra sikkerhedshændelser og for sikker 
og fortrolig opbevaring af personoplysninger i overensstemmelse med Previstos sikkerhedsforanstaltninger som 
beskrevet i bilag B.

2. **Fortrolighed:** Previsto skal sikre, at enhver person som Previsto har bemyndiget til at behandle 
personoplysninger (herunder dets personale, agenter og underentreprenører), er underlagt en passende 
fortrolighedsforpligtelse (hvad enten der er tale om en kontraktlig eller lovbestemt tjeneste).

3. **Underretning om sikkerhedsbrud:** Efter at være blevet opmærksom på et sikkerhedsbrud skal Previsto 
underrette Kunden uden unødig forsinkelse og skal fremlægge rettidig information vedrørende sikkerhedshændelsen, 
efterhånden som den bliver kendt eller anmodet om af kunden.

4. **Opdateringer til sikkerhedsforanstaltninger:** Kunden erkender, at sikkerhedsforanstaltningerne er 
underlagt teknisk udvikling og at Previsto kan opdatere eller ændre sikkerhedsforanstaltningerne fra tid til 
anden, forudsat at sådanne opdateringer og ændringer ikke medfører forringelse af den samlede sikkerhed for 
de tjenester som Kunden køber.



## Overførsel af oplysninger til tredjelande eller internationale organisationer

1. **Behandlingssteder:** Previsto gemmer og behandler EU-data (defineret nedenfor) i 
    datacentre beliggende inden for EU. 

## Bistand til Kunden

1. I det omfang kunden ikke er i stand til selvstændigt at få adgang til de relevante personoplysninger inden for 
tjenesterne, skal Previsto (på Kundens bekostning) tilbyde et rimeligt samarbejde for at bistå kunden med passende 
tekniske og organisatoriske foranstaltninger så vidt muligt, samt reagere på anmodninger fra enkeltpersoner eller 
gældende databeskyttelsesmyndigheder vedrørende behandling af personoplysninger i henhold til aftalen. I tilfælde af, 
at en sådan anmodning rettes direkte til Previsto, skal Previsto ikke svare direkte på denne meddelelse uden Kundens 
forudgående tilladelse, medmindre det er tvunget ved lov. Hvis Previsto skal svare på en sådan anmodning, skal 
Previsto straks underrette Kunden og give denne en kopi af anmodningen, medmindre det er forbudt ved lov.

2. I det omfang det kræves af Previsto i henhold til databeskyttelsesloven, skal Previsto (på Kundens bekostning) 
give adgang til oplysninger om Previstos behandling af personoplysninger i henhold til aftalen, for at gøre det muligt 
for kunden at udføre konsekvensanalyser for databeskyttelse eller konsultationer med databeskyttelsesmyndigheder 
som krævet i henhold til loven.


## Sletning og tilbagelevering af oplysninger

1.  Ved ophør af tjenesterne vedrørende behandling forpligtes
    Previsto til, efter Kundens valg, at slette eller
    tilbagelevere alle personoplysninger til Kunden, samt at
    slette eksisterende kopier, medmindre EU-retten eller national ret
    foreskriver opbevaring af personoplysningerne.


**Previsto Aps**  
**Name:** Michael Krog  
**Titel:** Direktør  

**Underskrift:** ______________________________    


## Bilag A - Liste over underdatabehandlere
Udleveres ved henvendelse

## Bilag B - Sikkerhedsforanstaltninger
Udleveres ved henvendelse
